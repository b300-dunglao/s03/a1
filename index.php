<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03 Activity</title>
</head>
<body>

	<h1>Person</h1>
	<p><?= $person->printName(); ?></p>

	<h1>Developer</h1>
	<p><?= $developer->printName(); ?></p>

	<h1>Engineer</h1>
	<p><?= $engineer->printName(); ?></p>


	<h1>Voltes V</h1>
	<p><?php var_dump($v1); ?></p>
	<p><?php var_dump($v2); ?></p>
	<p><?php var_dump($v3); ?></p>
	<p><?php var_dump($v4); ?></p>
	<p><?php var_dump($v5); ?></p>

	<h1>Voltes V</h1>

	<p><?= $v1->printName(); ?></p>
	<p><?= $v2->printName(); ?></p>
	<p><?= $v3->printName(); ?></p>
	<p><?= $v4->printName(); ?></p>
	<p><?= $v5->printName(); ?></p>


</body>
</html>