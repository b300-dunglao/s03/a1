<?php

class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}

}

$person = new Person("Senku", null, "Ishigami");


class Developer extends Person {

	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}

}

$developer = new Developer("John", "Finch", "Smith");

class Engineer extends Person {

	public function printName(){
		return "Your are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
}

$engineer = new Engineer("Harold", "Myers", "Reese");



class Character {
	public $name;


	public function __construct($name){
		$this->name = $name;

	}
	public function printName(){
		return "Hi, I am $name!";
	}
}


class VoltesMember extends Character {
	public $vehicle;

	public function __construct($name, $vehicle){
		parent::__construct($name);
		$this->vehicle = $vehicle;
		
	}

	public function printName(){
		return "Hi, I am $this->name! I pilot the $this->vehicle.";
	}
}

$v1 = new VoltesMember("Steve", "Volt Cruiser");
$v2 = new VoltesMember("Big Bert", "Volt Panzer");
$v3 = new VoltesMember("Little John", "Volt Frigate");
$v4 = new VoltesMember("Jamie", "Volt Lander");
$v5 = new VoltesMember("Mark", "Volt Bomber");